import { parseToISODate, isValidDate, getAge } from './date-utils.js';

const form = document.forms['ageInputForm'];
const dayControl = form.querySelector('.day-control');
const dayInput = form.elements['day'];
const monthControl = form.querySelector('.month-control');
const monthInput = form.elements['month'];
const yearControl = form.querySelector('.year-control');
const yearInput = form.elements['year'];
const yearsOutput = document.querySelector('#results .years > span');
const monthsOutput = document.querySelector('#results .months > span');
const daysOutput = document.querySelector('#results .days > span');

form.addEventListener('submit', formSubmitHandler);

// Show/hide error message when user inputs a day
dayInput.addEventListener('input', (e) => {
  const value = e.target.value;
  validateDay(value);
});

// Show/hide error message when user inputs a month
monthInput.addEventListener('input', (e) => {
  const value = e.target.value;
  validateMonth(value);
});

// Show/hide error message when user inputs a year
yearInput.addEventListener('input', (e) => {
  const value = e.target.value;
  validateYear(value);
});

function formSubmitHandler(e) {
  e.preventDefault();

  // Validate day, month, and year inputs
  const isDayValid = validateDay(dayInput.value);
  const isMonthValid = validateMonth(monthInput.value);
  const isYearValid = validateYear(yearInput.value);

  if (!isDayValid || !isMonthValid || !isYearValid) {
    console.error('Invalid input');
    return;
  }

  const dateISO = parseToISODate(yearInput.value, monthInput.value, dayInput.value);
  if (!isValidDate(dateISO)) {
    showOrHideDayError('Must be a valid date');
    return;
  }

  // Get the age, and display it
  const age = getAge(dateISO);
  yearsOutput.textContent = age.years;
  monthsOutput.textContent = age.months;
  daysOutput.textContent = age.days;
}

// Displays error message if day is invalid. Clears and hides error message if day is valid
function showOrHideDayError(message, isShow = true) {
  const dayError = document.getElementById('dayError');
  dayError.textContent = message;

  if (isShow) {
    dayError.classList.remove('hidden');
    dayControl.classList.add('invalid');
    return;
  }

  dayControl.classList.remove('invalid');
  dayError.classList.add('hidden');
}

// Displays error message if month is invalid. Clears and hides error message if month is valid
function showOrHideMonthError(message, isShow = true) {
  const monthError = document.getElementById('monthError');
  monthError.textContent = message;

  if (isShow) {
    monthError.classList.remove('hidden');
    monthControl.classList.add('invalid');
    return;
  }

  monthControl.classList.remove('invalid');
  monthError.classList.add('hidden');
}

// Displays error message if year is invalid. Clears and hides error message if year is valid
function showOrHideYearError(message, isShow = true) {
  const yearError = document.getElementById('yearError');
  yearError.textContent = message;

  if (isShow) {
    yearError.classList.remove('hidden');
    yearControl.classList.add('invalid');
    return;
  }

  yearControl.classList.remove('invalid');
  yearError.classList.add('hidden');
}

// Makes sure the day is not empty string or an invalid day
function validateDay(dayValue) {
  let isValid = true;
  if (!dayValue) {
    showOrHideDayError('This field is required');
    isValid = false;
  } else if (dayValue > 31 || dayValue < 1) {
    showOrHideDayError('Must be a valid day');
    isValid = false;
  } else {
    showOrHideDayError('', false);
  }
  return isValid;
}

// Makes sure the month is not empty string or an invalid month
function validateMonth(monthValue) {
  let isValid = true;
  if (!monthInput.value) {
    showOrHideMonthError('This field is required');
    isValid = false;
  } else if (monthValue > 12 || monthValue < 1) {
    showOrHideMonthError('Must be a valid month');
    isValid = false;
  } else {
    showOrHideMonthError('', false);
  }
  return isValid;
}

// Makes sure the year is not empty string or a future year
function validateYear(yearValue) {
  let isValid = true;
  const currentYear = new Date().getFullYear();

  if (!yearValue) {
    showOrHideYearError('This field is required');
    isValid = false;
  } else if (yearValue > currentYear) {
    showOrHideYearError('Must be in the past');
    isValid = false;
  } else {
    showOrHideYearError('', false);
  }
  return isValid;
}
