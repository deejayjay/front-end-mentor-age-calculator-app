const { DateTime } = luxon;

// Check if the given date is a valid date
function isValidDate(dateString) {
  return DateTime.fromISO(dateString).isValid;
}

// Accept the birth date in ISO format, and get the number of years, months, and days since that date
function getAge(birthDateISO) {
  const startDate = DateTime.now();
  const endDate = DateTime.fromISO(birthDateISO);

  // Get the interval between the birth date and now
  const interval = endDate.until(startDate);

  // Convert the interval to a duration object
  const durationObj = interval.toDuration(['years', 'months', 'days']).toObject();

  const ageObj = {
    years: Math.floor(durationObj.years),
    months: Math.floor(durationObj.months),
    days: Math.floor(durationObj.days)
  };

  return ageObj;
}

// This method takes in a year, month, and day and returns a ISO date string
function parseToISODate(year, month, day) {
  const dateTime = DateTime.fromFormat(`${year}-${month}-${day}`, 'yyyy-M-d');
  return dateTime.toISO();
}

export { isValidDate, getAge, parseToISODate };
